import json
import os
import boto3
from dotenv import load_dotenv
load_dotenv()


username = 'test'
password = 'password'


client = boto3.client('cognito-idp', region_name=os.getenv('COGNITO_REGION_NAME'))
response = client.initiate_auth(
        ClientId=os.getenv('COGNITO_USER_CLIENT_ID'),
        AuthFlow='USER_PASSWORD_AUTH',
        AuthParameters={
            'USERNAME': username,
            'PASSWORD': password
        }
    )

# Print the entire response dictionary
print(json.dumps(response, indent=4))

# Check for errors
if 'Error' in response:
    print(f"Error: {response['Error']['Message']}")
else:
    access_token = response['AuthenticationResult']['AccessToken']
    refresh_token = response['AuthenticationResult']['RefreshToken']
    cognito_user_id = response['AuthenticationResult']['IdToken']['Sub']

    print(access_token)
    print(refresh_token)
    print(cognito_user_id)
    